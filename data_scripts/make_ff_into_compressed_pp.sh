#!/usr/bin/bash

# Create a compressed PP file, using mule.
# This shell script exists to capture how we have to run with scitools+mule.

# Loading with scitools + mule, should probably be something like..
#     $ module load scitools/experimental-current
#     $ module load um_tools/next
#
# As-of 2020-02-03, that does not work, but the following does ...

module load scitools/experimental-current
module load um_tools/2019.01.1

python helper_code/make_ff_into_compressed_pp.py
